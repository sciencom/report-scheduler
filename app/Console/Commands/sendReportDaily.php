<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class sendReportDaily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:mail_send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email Daily';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user = User::all();
        foreach ($user as $a)
        {
            $this->info("send email to : $a->email");
            Mail::raw("This is automatically generated daily Update", function($message) use ($a)
            {
                $message->from(env('MAIL_FROM_ADDRESS'));
                $message->to($a->email)->subject('Daily Update');
            });
        }
            $this->info('Daily Report mails has been send successfully');
    }
}
